import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Navigation from './src/Navigation';
import { Store, Persistor } from './src/Redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';



function App() {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistor}>
        <NavigationContainer>
          <Navigation />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}

export default App;
