export const colors = {
  gold: '#DECB22',
  white: '#dfe3ee',
  red: 'crimson',
  green: '#49FF00',
  grey: '#818181',
  lightGrey: '#FFF9',
  darkGrey: '#242a37',
  darkGreyTransparent: '#33333380',
  primary: '#f9c678',
  secondary:'#ff2d55',
  black:'#333333',
  greyne:'#f1f2f6'
};
