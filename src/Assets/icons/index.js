import IconHome from './Home.svg';
import IconHomeActive from './Home-Active.svg';
import IconSearch from './Search.svg';
import IconSearchActive from'./Search-Active.svg';
import IconProfileActive from './Profile-Active.svg';
import IconProfile from './Profile.svg';
import IconSetting from './Setting.svg';
import IconSettingActive from './Setting-Active.svg';
import MessageKapster from './message.svg';
import NotifKapster from './notif.svg';
import MessageKapsterActive from './message-active.svg';
import NotifKapsterActive from './notif-active';
import IconUser from './User.svg';
import IconTerm from './Term.svg';
import IconLogout from './Logout.svg';
import IconArrowRight from './Arrow-Right.svg';

export {
    IconHome,
    IconHomeActive,
    IconSearch,
    IconSearchActive,
    IconProfileActive,
    IconProfile,
    IconSetting,
    IconSettingActive,
    MessageKapster,
    NotifKapster,
    MessageKapsterActive,
    NotifKapsterActive,
    IconUser,
    IconTerm,
    IconLogout,
    IconArrowRight,
};
