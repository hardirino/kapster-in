import Gambar from '../images/image1.png';
import Vektor from '../images/vector.png';
import Ellipse from '../images/ellipse.png';
import KapsterinText from '../images/kapsterinText.png';
import Message from '../images/message.png';
import Notification from '../images/notification.png';
import Kapster1 from '../images/kapster1.jpg';
import Kapster2 from '../images/kapster2.jpg';
import MenSpace from '../images/mens-space.jpg';
import SettingAvatar from '../images/SettingAvatar.png';
import Message2 from '../images/message.svg';
import Notification2 from '../images/notif.svg';
import KapsterinText2 from '../images/kapster.svg';
import IconTask from './Task.svg';
import IconUserBlue from './User-Blue.svg';
import IconTime from './Time.svg';
import DefaultImage from './DefaultImage.png'



export {
    Gambar, 
    Vektor, 
    Ellipse, 
    KapsterinText,
    Message,
    Notification,
    Kapster1,
    Kapster2,
    MenSpace,
    SettingAvatar,
    Message2,
    Notification2,
    KapsterinText2,
    IconTask,
    IconUserBlue,
    IconTime,
    DefaultImage,
}