import React, {Component} from 'react';
import {
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Text,
  Alert,
  View,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import logo from '../../Images/Logo-Gold.png';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {colors, fonts} from '../../utils/';

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
    };
  }

  _onRegist = () => {
    const {name, email, password} = this.state;
    if (name && email && password) {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async res => {
          await firestore().collection('users').doc(res.user.uid).set({
            name: name,
            email: email,
            role: 'user',
            uid: res.user.uid,
          });
          alert('Register Success');
        })
        .finally(() => {
          this.props.navigation.replace('Login');
        });
    } else {
      Alert.alert('Error', 'You need to input all of them');
    }
  };

  render() {
    const {name, email, password} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.mainContainer}>
        <MaterialComunnityIcon
          style={styles.arrowBack}
          name={'chevron-left'}
          size={40}
          color={colors.gold}
          onPress={() => navigation.goBack()}
        />
        <Image style={styles.logo} source={logo} />
        <Text style={styles.textWelcome}>WELCOME TO KAPSTERIN</Text>
        <Text style={styles.textCreate}>Create your account</Text>
        <View style={styles.inputParentWrapper}>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'account'}
              size={35}
              color={colors.darkGrey}
              style={{marginLeft: 15}}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Name"
              value={name}
              onChangeText={name => this.setState({name})}
            />
          </View>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'email'}
              size={35}
              color={colors.darkGrey}
              style={{marginLeft: 15}}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Email"
              value={email}
              onChangeText={email => this.setState({email})}
            />
          </View>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'lock'}
              size={35}
              color={colors.darkGrey}
              style={{marginLeft: 15}}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Password"
              secureTextEntry
              value={password}
              onChangeText={password => this.setState({password})}
            />
          </View>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this._onRegist();
          }}>
          <Text style={styles.buttonText}>SIGN UP</Text>
        </TouchableOpacity>
        <Text style={styles.textSignIn}>
          Already have an account?{' '}
          <Text
            onPress={() => navigation.navigate('Login')}
            style={{color: colors.red}}>
            Sign In!
          </Text>
        </Text>
        <Text style={{...styles.textSignIn, marginTop: 5}}>
          Want to be a Kapster?{' '}
          <Text
            onPress={() => navigation.navigate('RegisterKapster')}
            style={{color: colors.red}}>
            Join!
          </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.darkGrey,
    alignItems: 'center',
  },
  arrowBack: {
    marginLeft: 10,
    marginTop: 20,
    alignSelf: 'flex-start',
  },
  logo: {
    height:'20%',
    width: '40%',
    resizeMode:'contain'
  },
  textWelcome: {
    fontSize: 23,
    fontFamily: fonts.bold,
    color: colors.gold,
    marginTop: 20,
  },
  textCreate: {
    color: colors.gold,
    fontFamily:fonts.regular
  },
  inputParentWrapper: {
    width: '90%',
    height: 275,
    alignItems: 'center',
    backgroundColor: colors.gold,
    borderRadius: 15,
    marginTop: 20,
    paddingVertical: 15,
    elevation: 10,
  },
  inputWrapper: {
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: colors.darkGrey,
    width: '87%',
  },
  inputText: {
    fontSize: 23,
    fontWeight: '500',
    color: colors.darkGrey,
    fontFamily: fonts.regular,
    marginLeft: 10,
    width:'80%'
  },
  button: {
    backgroundColor: colors.gold,
    width: '25%',
    height: '5%',
    borderRadius: 10,
    marginTop: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: colors.darkGrey,
    fontFamily:fonts.bold
  },
  textSignIn: {
    color: colors.white,
    marginTop: 40,
    fontFamily:fonts.semiBold
  },
});
