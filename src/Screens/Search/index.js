import React, { Component } from 'react';
import {
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableHighlight,
  Alert,
  ScrollView,
} from 'react-native';
import { Gambar, Vektor } from '../../Assets';
import Icon from 'react-native-vector-icons/Entypo';
import firestore from '@react-native-firebase/firestore';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { colors } from '../../utils';
export default class Search extends Component {
  constructor() {
    super();
    this.state = {
      isClick: 0,
      dataKapster: [],
      search: [],
    };
  }

  componentDidMount() {
    firestore()
      .collection('users')
      .where('role', '==', 'kapster')
      .get()
      .then(value => {
        this.setState({
          dataKapster: value.docs.map(result => {
            return result.data();
          }),
        });
      });
  }

  _searchKapster = value => {
    const { dataKapster } = this.state;
    const newData = dataKapster.filter(v => {
      const vData = `${v.name.toUpperCase()}`;
      const valueData = value.toUpperCase();
      return vData.indexOf(valueData) > -1;
    });
    if (value.length > 0) {
      this.setState({ search: newData });
    } else {
      this.setState({ search: [] });
    }
  };

  render() {
    const { isClick, search, dataKapster } = this.state;
    const { navigation } = this.props;
    // console.log(isClick);
    return (
      <View style={{ flex: 1, backgroundColor: colors.darkGrey, alignItems: 'center' }}>
        <View style={{marginTop:5,}}>
          <TextInput
            placeholder="Search"
            style={styles.textInput}
            onChangeText={value => this._searchKapster(value)}
            // textAlign='center'
            fontWeight='bold'
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            justifyContent: 'space-around',
          }}>
          {/* <TouchableHighlight
            style={isClick == 1 ? styles.tabsActive : styles.tabsNonActive}
            onPress={() => this.setState({isClick :1 })}>
            <Text
              style={isClick == 1 ? styles.textActive : styles.textNonActive}>
              Price
            </Text>
          </TouchableHighlight> */}
          {/* <TouchableOpacity
            style={
              this.state.isClick == 2 ? styles.tabsActive : styles.tabsNonActive
            }
            onPress={() => this.setState({isClick: 2})}>
            <Text
              style={
                this.state.isClick == 2
                  ? styles.textActive
                  : styles.textNonActive
              }>
              Experience
            </Text>
          </TouchableOpacity> */}
          {/* <TouchableOpacity
            style={
              this.state.isClick == 3 ? styles.tabsActive : styles.tabsNonActive
            }
            onPress={() => this.setState({isClick: 3})}>
            <Text
              style={
                this.state.isClick == 3
                  ? styles.textActive
                  : styles.textNonActive
              }>
              City
            </Text>
          </TouchableOpacity> */}
        </View>
        <ScrollView style={{ marginBottom: 75, }} showsVerticalScrollIndicator={false}>
          {search.map((v, i) => {
              return (
                <TouchableOpacity key={i} style={styles.favourite}
                  onPress={() =>
                    navigation.navigate('OrderKapster', { dataKapster: v.uid })
                  }>
                  <Image
                    source={{ uri: v.imageDisplay }}
                    style={{ width: 93, height: 85 }}
                  />
                  <View
                    style={{ justifyContent: 'space-evenly', marginLeft: 20 }}>
                    <Text style={{ fontWeight: '700', color: 'white' }}>
                      {v.name}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{ fontWeight: '400', color: 'white' }}>
                        Rp.{v.price}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'flex-end',
                          marginLeft: 20,
                        }}>
                        <Icon
                          name="star"
                          size={20}
                          style={{ color: 'yellow' }}
                        />
                        <Text
                          style={{
                            fontWeight: '400',
                            color: 'white',
                            marginLeft: 5,
                          }}>
                          4.8
                        </Text>
                      </View>
                    </View>
                    {/* <Image source={v.imageDisplay} style={{width: 10, height: 10}} /> */}
                  </View>
                </TouchableOpacity>
              );
            })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    marginTop: 22,
    marginBottom: 27,
    padding: 10,
    backgroundColor: '#818181',
    width: 390,
    height: 42,
    borderRadius: 10,
    fontSize:18,
   },
  tabsActive: {
    backgroundColor: colors.darkGrey,
    borderWidth: 1,
    width: 110,
    borderRadius: 5,
    borderColor: 'gold',
    paddingHorizontal: 10,
    paddingVertical: 3,
    marginBottom: 10,
    alignItems: 'center',
  },
  tabsNonActive: {
    backgroundColor: colors.darkGrey,
    borderWidth: 1,
    width: 110,
    borderRadius: 5,
    borderColor: 'white',
    paddingHorizontal: 10,
    paddingVertical: 3,
    marginBottom: 10,
    alignItems: 'center',
  },
  textActive: {
    color: 'gold',
  },
  textNonActive: {
    color: 'white',
  },
  favourite: {
    flexDirection: 'row',
    backgroundColor: '#818181',
    width: 360,
    height: 85,
    borderRadius: 15,
    marginTop: 20,
    elevation: 10,
    shadowColor: '#52006A',
  },
});