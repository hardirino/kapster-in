import Home from './Home';
import Search from './Search';
import Setting from './Setting';
import Splash from '../Screens/Splash';
import Register from '../Screens/Register';
import Login from '../Screens/Login';
import ForgetPassword from '../Screens/ForgetPassword';
import Message from '../Screens/Message';
import User from '../Screens/User';
import TabNavigation from '../Components/Huge/TabNavigation';
import RegisterKapsterInfo from '../Screens/RegisterKapsterInfo';
import Account from '../Screens/Account'
import PrivacyPolicy from '../Screens/PrivacyPolicy';
import OrderKapster from '../Screens/OrderKapster'
import RegisterKapster from '../Screens/RegisterKapster';
import Chat from '../Screens/Chat'
import NotifScreen from './NotifScreen';
import onChat from './onChat';

export {
    Home,
    Search,
    Setting,
    Splash,
    Register,
    Login,
    ForgetPassword,
    Message,
    User,
    TabNavigation,
    RegisterKapster,
    RegisterKapsterInfo,
    PrivacyPolicy,
    OrderKapster,
    Account,
    Chat,
    NotifScreen,
    onChat,
}