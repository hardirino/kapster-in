import {Text, StyleSheet, View} from 'react-native';
import React, {Component} from 'react';
import {colors, fonts} from '../../utils';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataKapster: [],
    };
  }

  

  render() {
    console.log(this.props.route.params);
    const {navigation} = this.props;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerWrapper}>
          <MaterialComunnityIcon
            name={'chevron-left'}
            size={40}
            color={colors.gold}
            onPress={() => navigation.goBack()}
          />
          <Text style={styles.text}>Order</Text>
          <View style={{width: 50}} />
        </View>
        <View>
            
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.darkGrey,
  },
  headerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '6%',
  },
  text: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 20,
    marginVertical: 5,
    marginHorizontal: 20,
    textAlign: 'justify',
  },
});
