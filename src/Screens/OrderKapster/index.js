import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import firestore from '@react-native-firebase/firestore';
import {colors, fonts, getUniqueCode, toastAndroidCenter} from '../../utils';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import _objectWithoutProperties from '@babel/runtime/helpers/objectWithoutProperties';
import {connect} from 'react-redux';

class OrderKapster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataKapster: [],
    };
  }

  componentDidMount() {
    const get = this.props.route.params;
    firestore()
      .collection('users')
      .doc(get.dataKapster)
      .get()
      .then(documentSnapshot => {
        this.setState({dataKapster: documentSnapshot.data()});
      });
  }

  _onPayment = async () => {
    const get = this.props.route.params;
    const {dataUser} = this.props;
    const {dataKapster} = this.state;
    let idMix = `${dataUser.uid.substr(24)}${dataKapster.uid.substr(24)}`;
    const ordersID = getUniqueCode(new Date(), idMix);
    await firestore()
      .collection('orders')
      .doc(ordersID)
      .set({
        idUser: dataUser.uid,
        idKapster: get.dataKapster,
        dateOrder: new Date(),
        ordersID: ordersID,
        user: dataUser.name,
      })
      .then(() => {
        toastAndroidCenter('Order success');
        this.props.navigation.replace('onChat', dataKapster);
      });
  };

  render() {
    const {navigation} = this.props;
    const {dataKapster} = this.state;
    console.log(getUniqueCode);
    return (
      <View style={styles.mainContainer}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: '6%',
          }}>
          <MaterialComunnityIcon
            name={'chevron-left'}
            size={40}
            color={colors.gold}
            onPress={() => navigation.goBack()}
          />
          <Text style={{...styles.text, fontSize: 29}}>{dataKapster.name}</Text>
          <View style={{width: 50}} />
        </View>
        <Image source={{uri: dataKapster.imageDisplay}} style={styles.image} />
        <Text style={{...styles.text, marginTop:15,fontSize: 25, color: colors.secondary}}>
          Rp.{dataKapster.price}
        </Text>
        <Text
          style={{
            ...styles.text,
            fontSize: 16,
            marginTop: 3,
            fontFamily: fonts.medium,
            marginBottom: 5,
          }}>
          {dataKapster.address}
        </Text>
        <View
          style={{
            borderTopWidth: 2,
            width: '100%',
            borderColor: colors.grey,
            marginTop: 10,
          }}>
          <Text
            style={{
              ...styles.text,
              fontFamily: fonts.regular,
              marginTop: 20,
            }}>
            {dataKapster.bio}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: colors.grey,
            height: 50,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            position: 'absolute',
            bottom: 0,
          }}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this._onPayment();
            }}>
            <Text style={styles.buttonText}>Order</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{...styles.button, backgroundColor: colors.grey}}
            onPress={() => {
              navigation.navigate('onChat', dataKapster);
            }}>
            <Text style={styles.buttonText}>Chat Kapster</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataUser: state.userReducer.dataUser,
  };
};

export default connect(mapStateToProps)(OrderKapster);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.darkGrey,
  },
  contentWrapper: {
    height: '80%',
    width: '95%',
    borderRadius: 15,
    marginTop: 15,
  },
  image: {
    width: '100%',
    height: 300,
  },
  textWrapper: {
    flex: 1,
    width: '100%',
    padding: 20,
  },
  text: {
    fontFamily: fonts.bold,
    color: colors.white,
    fontSize: 15,
    marginVertical: 5,
    marginHorizontal: 20,
    textAlign: 'justify',
  },
  button: {
    backgroundColor: colors.gold,
    width: '50%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: colors.darkGrey,
    fontFamily: fonts.bold,
  },
});
