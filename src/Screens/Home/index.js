import React, { Component } from 'react';
import {
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  Linking,
  StatusBar
} from 'react-native';
import {
  KapsterinText,
  MenSpace,
  Kapster1,
} from '../../Assets';
import { BannerSlider } from '../../Components';
import firestore from '@react-native-firebase/firestore';
import { colors, fonts } from '../../utils';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataKapster: [],
    };
  }

  componentDidMount() {
    firestore()
      .collection('users')
      .where('role', '==', 'kapster')
      .onSnapshot(value => {
        this.setState({
          dataKapster: value.docs.map(result => {
            return result.data();
          }),
        });
      });
  }

  titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  render() {
    const { dataKapster } = this.state;
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.darkGrey} />
        <View style={styles.container1}>
          <View>
            <Image
              style={{ width: 150, height: 40 }}
              resizeMode="contain"
              source={KapsterinText}
            />
          </View>
        </View>
        <View style={styles.containerText}>
          <Text style={styles.Text1}>Most Popular</Text>
        </View>
        <View style={styles.page}>
          <BannerSlider />
          <View style={styles.textCategoriesWrapper}></View>
          <View style={styles.circleCategories}></View>
        </View>
        <View style={styles.containerText1}>
          <Text style={styles.Text1}>List Kapsters</Text>
        </View>
        <ScrollView
          style={styles.scrollview}
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          {dataKapster.map((v, i) => {
            return (
              <TouchableOpacity key={i} style={styles.containerPopular} onPress={() =>
                navigation.navigate('OrderKapster', { dataKapster: v.uid })
              }>
                <Image
                  style={styles.kapsters}
                  resizeMode="cover"
                  source={v.imageDisplay ? { uri: v.imageDisplay } : Kapster1}
                />
                <View style={styles.textKapster}>
                  <Text style={{ fontFamily: fonts.bold, color: 'white' }}>
                    {this.titleCase(v.name)}
                  </Text>
                  <Text
                    style={{
                      fontFamily: fonts.regular,
                      color: 'white',
                      marginRight: 10,
                    }}>
                    Rp.{v.price}
                  </Text>
                  {v.imageVaccine ? (
                    <View>
                      <MaterialComunnityIcon
                        name={'shield'}
                        color={colors.green}
                      />
                    </View>
                  ) : null}

                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <TouchableOpacity style={styles.mensContainer} onPress={() => Linking.openURL('http://thebarbermagazine.co.uk/')}>
          <ImageBackground
            source={MenSpace}
            borderRadius={15}
            blurRadius={5}
            style={styles.mensImage}>
            <Text style={styles.mensSpaceText}>Men's Space</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.darkGrey,
  },
  container1: {
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'center'
  },
  message: {
    marginTop: 7,
    marginLeft: '47%',
  },
  page: {
    height: '35%',
    marginLeft: 15,
  },
  notif: {
    marginLeft: '40%',
    marginTop: 5,
  },
  containerText: {
    marginLeft: 26,
    marginTop: 10,
    marginBottom: 10,
  },
  containerText1: {
    marginLeft: 26,
    marginTop: 9,
    marginBottom: '4%'
  },
  Text1: {
    color: colors.white,
    fontSize: 20,
    fontFamily: fonts.bold,
    marginTop: '2%'
  },
  textKapster: {
    marginLeft: 15,
    marginVertical: 15,
    justifyContent: 'space-between',
  },
  scrollview: {
    maxHeight: '12%',
    marginLeft: 5,
    marginRight: 5,
  },
  containerPopular: {
    backgroundColor: colors.grey,
    height: 100,
    width: 250,
    borderRadius: 15,
    marginHorizontal: 13,
    flexDirection: 'row',
    elevation: 3,
  },
  kapsters: {
    height: 100,
    width: 110,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  mensContainer: {
    alignSelf: 'center',
    borderRadius: 15,
    height: 140,
    width: '90%',
    marginTop: 20,
  },
  mensImage: {
    justifyContent: 'center',
    width: '100%',
    height: 150,
    justifyContent: 'center',
    marginTop: '3%'
  },
  mensSpaceText: {
    alignSelf: 'center',
    fontSize: 30,
    color: colors.lightGrey,
    fontFamily: fonts.regular,
  },
});
