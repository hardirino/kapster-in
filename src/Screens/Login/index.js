import React, { Component } from 'react';
import {
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  StatusBar,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import logo from '../../Images/Logo-Gold.png';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { colors, fonts } from '../../utils';
import { connect } from 'react-redux';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  _OnLogin = () => {
    const { email, password } = this.state;

    const { navigation, userLogin } = this.props;
    let user;
    if (email != '' && password != '') {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          firestore()
            .collection('users')
            .doc(res.user.uid)
            .onSnapshot(value => {
              user = value.data();
              userLogin(user);
              this.setState({ isLoading: false });
              if (user.role === 'kapster') {
                navigation.replace('KapsterHome');
              } else {
                navigation.replace('MainHome');
              }
            });
        })
        .catch(error => {
          if (error.code === 'auth/user-not-found') {
            ToastAndroid.show('User not found !', ToastAndroid.SHORT);
          } else if (error.code === 'auth/wrong-password') {
            ToastAndroid.show('Wrong password !', ToastAndroid.SHORT);
          } else if (error.code === 'auth/invalid-email') {
            ToastAndroid.show('Email address invalid !', ToastAndroid.SHORT);
          } else {
            console.log(error);
          }
        });
    } else {
      ToastAndroid.show('Please fill the input !', ToastAndroid.SHORT);
    }
  };

  render() {
    const { navigation } = this.props;
    const { email, password } = this.state;
    return (
      <View style={styles.mainContainer}>
        <StatusBar backgroundColor={colors.darkGrey}/>
        <Image style={styles.logo} source={logo} />
        <Text style={styles.textWelcome}>HELLO!</Text>
        <Text style={styles.textCreate}>
          Please input your email & password to Login
        </Text>
        <View style={styles.inputParentWrapper}>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'email'}
              size={35}
              color={colors.darkGrey}
              style={{ marginLeft: 15 }}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Email"
              value={email}
              onChangeText={email => this.setState({ email })}
            />
          </View>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'lock'}
              size={35}
              color={colors.darkGrey}
              style={{ marginLeft: 15 }}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Password"
              secureTextEntry={true}
              value={password}
              onChangeText={password => this.setState({ password })}
            />
          </View>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this._OnLogin();
          }}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
        <Text style={styles.textSignIn}>
          Don’t have an account?{' '}
          <Text
            onPress={() => navigation.navigate('Register')}
            style={{ color: colors.red }}>
            Sign Up!
          </Text>
        </Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin,
  };
};

const mapDispatchToProps = send => {
  return {
    userLogin: data =>
      send({
        type: 'USER-LOGIN',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.darkGrey,
    alignItems: 'center',
  },
  arrowBack: {
    marginLeft: 10,
    marginTop: 20,
    alignSelf: 'flex-start',
  },
  logo: {
    marginTop:20,
    height: '25%',
    width: '40%',
    resizeMode:'contain'
  },
  textWelcome: {
    fontSize: 23,
    color: colors.gold,
    marginTop: 10,
    fontFamily: fonts.bold,
  },
  textCreate: {
    color: colors.gold,
    fontFamily: fonts.regular,
    marginTop:10,
  },
  inputParentWrapper: {
    width: '90%',
    height: 250,
    alignItems: 'center',
    backgroundColor: colors.gold,
    borderRadius: 15,
    marginTop: 30,
    paddingVertical: 25,
  },
  inputWrapper: {
    marginVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: colors.darkGrey,
    width: '87%',
  },
  inputText: {
    fontSize: 23,
    fontFamily: fonts.regular,
    color: colors.darkGrey,
    marginLeft: 10,
    width: '80%',
  },
  button: {
    backgroundColor: colors.gold,
    width: '25%',
    height: '5%',
    borderRadius: 10,
    marginTop: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: colors.darkGrey,
    fontFamily: fonts.bold
  },
  textSignIn: {
    color: colors.white,
    fontFamily: fonts.semiBold,
    marginTop: '30%'
  },
});
