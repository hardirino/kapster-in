import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import { Ellipse } from '../../Assets/images';
import Phone from 'react-native-vector-icons/Ionicons';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Logout from 'react-native-vector-icons/MaterialIcons';

export default class User extends Component {

    render() {
        const { navigation } = this.props;
        return (
            <View style={{ flex: 1, backgroundColor: '#333333', }} >

                <View style={{ alignItems: 'center' }}>
                    <Image source={Ellipse} style={{ marginVertical: 30, }} />
                </View>

                <View style={styles.wrap}>
                    <View style={{ flexDirection: 'row', borderBottomWidth: 1, paddingVertical: 10 }}>
                        <MCI name="account-outline" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10,marginTop: 5 }}>Username</Text>
                    </View>
                    <View style={{ flexDirection: 'row', borderBottomWidth: 1, paddingVertical: 10 }}>
                        <Phone name="phone-portrait-sharp" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10,marginTop: 5 }}>+62xxxxxxx</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                        <MCI name="email" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10,marginTop: 5 }}>emailuser@email.com</Text>
                    </View>
                </View>

                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row' }}>
                        <MCI name="clipboard-text-clock" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10,marginTop: 5 }}>Order History</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row' }}>
                        <MCI name="star-box" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10,marginTop: 5 }}>My Favourite Kapster</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row' }}>
                        <MCI name="wallet" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10,marginTop: 5 }}>My Wallet</Text>
                    </View>
                </TouchableOpacity>

                <View style={{alignSelf:'center', width:'90%', flexDirection:'row', justifyContent:'flex-end'}}>
                    <TouchableOpacity onPress={()=> navigation.navigate('Login')} 
                    style={{
                        borderRadius:5,
                        backgroundColor: 'red',
                        marginTop: 43,
                    }}>
                        <Logout name="logout" size={30} color="#333333" />
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrap: {
        alignSelf: 'center',
        backgroundColor: '#818181',
        paddingHorizontal: 20,
        width: '90%',
        borderRadius: 15
    },
    button: {
        alignSelf: 'center',
        backgroundColor: '#818181',
        marginTop: 15,
        width: '90%',
        borderRadius: 15,
        padding: 15,
    },
})
