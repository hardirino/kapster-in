import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import React, { Component } from 'react';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import ImagePicker from 'react-native-image-crop-picker';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { colors, fonts } from '../../utils';

export default class RegisterKapsterInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bio: '',
      address: '',
      price: '',
      imageDisplay: '',
      changeImageDisplay: '',
      changeImageCertificate: '',
      changeImageVaccine: '',
    };
  }

  _pickImageDisplay = () => {
    ImagePicker.openPicker({
      width: 600,
      height: 400,
      cropping: true,
    })
      .then(image => {
        this.setState({ imageDisplay: image.path });
        this.setState({ changeImageDisplay: image.modificationDate });
      })
      .catch(e => {
        console.error(e);
      });
  };

  _pickImageVaccine = () => {
    ImagePicker.openPicker({
      width: 600,
      height: 300,
      cropping: true,
    })
      .then(image => {
        this.setState({ imageVaccine: image.path });
        this.setState({ changeImageVaccine: image.modificationDate });
      })
      .catch(e => {
        console.error(e);
      });
  };

  _pickImageCertificate = () => {
    ImagePicker.openPicker({
      width: 600,
      height: 400,
      cropping: true,
    })
      .then(image => {
        this.setState({ imageCertificate: image.path });
        this.setState({ changeImageCertificate: image.modificationDate });
      })
      .catch(e => {
        console.error(e);
      });
  };

  _register = () => {
    const { imageCertificate, imageDisplay, imageVaccine, bio, address, price } =
      this.state;
    const { name, email, password } = this.props.route.params;
    let urlImageDisplay, urlImageVaccine, urlImageCertificate;
    if (bio && address && price && name && email && password) {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async res => {
          await storage()
            .ref(`users/${res.user.uid}/Display.jpg`)
            .putFile(imageDisplay);
          await storage()
            .ref(`users/${res.user.uid}/Display.jpg`)
            .getDownloadURL()
            .then(res => (urlImageDisplay = res));
          await storage()
            .ref(`users/${res.user.uid}/Vaccine.jpg`)
            .putFile(imageVaccine);
          await storage()
            .ref(`users/${res.user.uid}/Vaccine.jpg`)
            .getDownloadURL()
            .then(res => (urlImageVaccine = res));
          await storage()
            .ref(`users/${res.user.uid}/Certificate.jpg`)
            .putFile(imageCertificate);
          await storage()
            .ref(`users/${res.user.uid}/Certificate.jpg`)
            .getDownloadURL()
            .then(res => (urlImageCertificate = res));
          await firestore()
            .collection('users')
            .doc(res.user.uid)
            .set({
              name: name,
              email: email,
              uid: res.user.uid,
              address: address,
              imageDisplay: urlImageDisplay,
              imageVaccine: urlImageVaccine,
              imageCertificate: urlImageCertificate,
              bio: bio,
              price: Number(price),
              role: 'kapster',
            });
        })
        .then(() => alert('Register Success!'))
        .finally(() => {
          this.props.navigation.replace('Login');
        })
        .catch(e => {
          console.error(e);
        });
    } else {
      Alert.alert('Error', 'You need to input all of them.');
    }
  };

  render() {
    const { navigation } = this.props;
    const {
      bio,
      price,
      address,
      changeImageDisplay,
      changeImageCertificate,
      changeImageVaccine,
    } = this.state;
    return (
      <View style={styles.mainContainer}>
        <MaterialComunnityIcon
          style={styles.arrowBack}
          name={'chevron-left'}
          size={40}
          color={colors.gold}
          onPress={() => navigation.goBack()}
        />
        <View style={styles.contain0}>
          <Text style={styles.headText}>Register as a KAPSTER</Text>
        </View>
        <View style={styles.contain1}>
          <Text style={{ ...styles.headText, fontSize: 14, marginTop: -5 }}>
            Please input the following information below:
          </Text>
        </View>
        <ScrollView
          style={{ width: '95%', marginLeft: 20 }}
          showsVerticalScrollIndicator={false}>
          <View style={styles.InputTextWrapper}>
            <View
              style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
              <Text style={styles.textSubHead}>Picture on display</Text>
              <TouchableOpacity
                style={styles.addImage}
                onPress={() => this._pickImageDisplay()}>
                <MaterialComunnityIcon
                  name={changeImageDisplay ? 'check-circle' : 'upload'}
                  size={40}
                  color={changeImageDisplay ? colors.green : colors.darkGrey}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
              <Text style={styles.textSubHead}>Vaccine certificate</Text>
              <TouchableOpacity
                style={styles.addImage}
                onPress={() => this._pickImageVaccine()}>
                <MaterialComunnityIcon
                  name={changeImageVaccine ? 'check-circle' : 'upload'}
                  size={40}
                  color={changeImageVaccine ? colors.green : colors.darkGrey}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
              <Text style={styles.textSubHead}>Bio</Text>
              <TextInput
                style={styles.inputBox}
                placeholder="Fill your bio"
                value={bio}
                onChangeText={bio => this.setState({ bio })}
              />
            </View>
            <View
              style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
              <Text style={styles.textSubHead}>Address</Text>
              <TextInput
                style={styles.inputBox}
                placeholder="Fill your address"
                value={address}
                onChangeText={address => this.setState({ address })}
              />
            </View>
            <View
              style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
              <Text style={styles.textSubHead}>Price</Text>
              <TextInput
                style={styles.inputBox}
                placeholder="Service price (IDR)"
                type="numeric"
                value={price}
                onChangeText={price => this.setState({ price })}
                keyboardType="numeric"
              />
            </View>
            <View
              style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
              <Text style={styles.textSubHead}>Barber certificate</Text>
              <TouchableOpacity
                style={styles.addImage}
                onPress={() => this._pickImageCertificate()}>
                <MaterialComunnityIcon
                  name={changeImageCertificate ? 'check-circle' : 'upload'}
                  size={40}
                  color={
                    changeImageCertificate ? colors.green : colors.darkGrey
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this._register();
          }}>
          <Text style={styles.buttonText}>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.darkGrey,
    alignItems: 'center',
  },
  arrowBack: {
    marginLeft: 10,
    marginTop: 20,
    alignSelf: 'flex-start',
  },
  contain0:{
  },
  contain1:{
    marginTop:30,
    marginBottom:10,
  },
  headText: {
    marginTop: -35,
    marginBottom: 5,
    color: colors.gold,
    fontSize: 23,
    fontFamily: fonts.bold
  },
  InputTextWrapper: {
    height: 630,
    width: '95%',
    backgroundColor: colors.gold,
    borderRadius: 15,
    paddingTop: 10,
  },
  textSubHead: {
    alignSelf: 'flex-start',
    marginLeft: 20,
    fontSize: 16,
    color: colors.darkGrey,
    marginBottom: 10,
    fontFamily: fonts.semiBold
  },
  addImage: {
    width: '90%',
    height: 50,
    borderRadius: 10,
    backgroundColor: colors.darkGreyTransparent,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputBox: {
    backgroundColor: colors.darkGreyTransparent,
    borderRadius: 10,
    width: '90%',
    fontFamily: fonts.regular,
    textAlign:'center'
  },
  button: {
    backgroundColor: colors.gold,
    width: '25%',
    height: '5%',
    borderRadius: 10,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
    flexDirection: 'row',
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 20,
    color: colors.darkGrey,
    fontFamily: fonts.bold
  },
});
