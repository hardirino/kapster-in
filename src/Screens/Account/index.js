import { Image, TouchableOpacity, StyleSheet, ImageBackground, Text, View } from 'react-native'
import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { dummyMenu } from '../../data/DummyMenu';
import { CCardMenu } from '../../Components'
import { colors } from '../../utils';



class Account extends Component {
    constructor(props) {
        super(props);

        this.state = {
            menu: dummyMenu,
            fullName: '',
            email: '',
        };
    }

    componentDidMount() {
        const { dataUser } = this.props;
        firestore()
            .collection('users')
            .doc(dataUser.uid)
            .onSnapshot(data => {
                this.setState({
                    fullName: data.data().name,
                    email: data.data().email,
                });
            });
    }

    _navigateMenu = menu => {
        const { navigation, userLogout } = this.props;
        if (menu.name === 'Logout') {
            auth()
                .signOut()
                .then(() => {
                    userLogout();
                    navigation.reset({
                        index: 1,
                        routes: [{ name: 'Login' }]
                    })
                    // navigation.replace('Login');
                });
        } else {
            navigation.navigate(menu.Screens);
        } console.log(userLogout)
    };

    render() {
        const { navigation } = this.props;
        const { fullName, email, menu } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkGrey, }} >
                <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginLeft: 10, marginTop: 15, marginBottom: 10, }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name="left" size={30} style={{ color: 'gold' }} />
                    </TouchableOpacity>
                    <Text style={{ color: 'gold', fontSize: 15, marginTop: 5, marginLeft: 130, fontWeight: 'bold' }}> Account </Text>
                </View>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                        <MCI name="account-outline" style={{ marginTop: 5, marginRight: 5 }} size={30} color="gold" />
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ color: 'white', marginLeft: 10, marginTop: 5, fontWeight: 'bold' }}>Full Name</Text>
                            <Text style={{ color: 'white', marginLeft: 10, }}>{fullName}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                        <Ionicons name="phone-portrait-outline" style={{ marginTop: 5, marginRight: 5 }} size={30} color="gold" />
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={{ color: 'white', marginLeft: 10, marginTop: 5, fontWeight: 'bold' }}>Email</Text>
                            <Text style={{ color: 'white', marginLeft: 10, }}>{email}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{ alignSelf: 'center', width: '90%', flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {menu.map(menu => {
                        return (
                            <CCardMenu
                                menu={menu}
                                key={menu.id}
                                onPress={() => this._navigateMenu(menu)}
                            />
                        );
                    })}
                </View>
            </View>
        )
    }
}
const mapStateToProps = state => {
    return {
        dataUser: state.userReducer.dataUser,
    };
};

const mapDispatchToProps = send => {
    return {
        userLogout: data =>
            send({
                type: 'USER-LOGOUT',
            }),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);

const styles = StyleSheet.create({
    button: {
        alignSelf: 'center',
        backgroundColor: '#818181',
        marginTop: 15,
        width: '90%',
        borderRadius: 15,
        padding: 10,
    },
})
