import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import Send from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Chat extends Component {
    render() {
        const {navigation} = this.props
        return (
            <View style={{ flex: 1, backgroundColor: '#333333' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 10, marginTop: 5 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name="left" size={30} style={{ color: 'gold' }} />
                    </TouchableOpacity>
                    <Text style={{ color: '#FFFFFF', fontSize: 15, marginBottom: 10 }}> Lionel Messi </Text>
                    <View></View>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, backgroundColor: '#818181', }}>

                        <View style={{
                            maxWidth: '85%', maxHeight:100, padding: 10, backgroundColor: 'white', marginTop: 15, alignSelf: 'flex-start', marginLeft: 8, borderTopStartRadius: 15,
                            borderTopEndRadius: 15,
                            borderBottomEndRadius: 15
                        }}>
                            <Text>Hallo, lalal
                                bfjbf
                                fmfif,ls ml ,cscm ,l momcs, lsmcls, jnjcsj
                                bfjbf
                                fmfif,ls ml ,cscm ,l momcs, lsmcls, jnjcsj
                                bfjbf
                                fmfif,ls ml ,cscm ,l momcs, lsmcls, jnjcsj
                                bfjbf
                                fmfif,ls ml ,cscm ,l momcs, lsmcls, jnjcsj
                            </Text>
                        </View>

                        <View style={{
                            maxWidth: '85%', maxHeight:100, padding: 10, backgroundColor: 'yellow', marginTop: 9, alignSelf: 'flex-end', marginRight: 8, borderTopStartRadius: 15,
                            borderTopEndRadius: 15,
                            borderBottomStartRadius: 15
                        }}>
                            <Text>Iya</Text>
                        </View>

                        <View style={{
                            maxWidth: '85%', maxHeight:100, padding: 10, backgroundColor: 'white', marginTop: 15, alignSelf: 'flex-start', marginLeft: 8, borderTopStartRadius: 15,
                            borderTopEndRadius: 15,
                            borderBottomEndRadius: 15
                        }}>
                            <Text>Iaa Doank ? </Text>
                        </View>

                        <View style={{
                            maxWidth: '85%', maxHeight:100, padding: 10, backgroundColor: 'yellow', marginTop: 9, alignSelf: 'flex-end', marginRight: 8, borderTopStartRadius: 15,
                            borderTopEndRadius: 15,
                            borderBottomStartRadius: 15
                        }}>
                            <Text>Yupzz, mau gmna lg ? xixixi</Text>
                        </View>

                    </View>
                    <View style={{
                        backgroundColor: '#333333',
                        height: 42,
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        marginTop: 7,
                    }}>
                        <TextInput style={{
                            backgroundColor: '#818181',
                            width: '85%',
                            height: '80%',
                            padding: 4,
                            borderRadius: 10,
                            color: 'white',
                            fontStyle:'italic',
                            paddingLeft:15,
                            fontSize:18
                            
                        }}>Message</TextInput>
                        <TouchableOpacity>
                            <Send name="send" size={30} style={{ color: 'gold' }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

