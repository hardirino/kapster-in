import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import { SettingAvatar } from '../../Assets/images';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Logout from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { colors } from '../../utils';


export default class Setting extends Component {
    render() {
        const { navigation } = this.props;
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkGrey, }} >
                <View style={{ alignItems: 'center' }}>
                    <Image source={SettingAvatar} style={{ marginVertical: 50, }} />
                </View>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Account')}>
                    <View style={{ flexDirection: 'row' }}>
                        <MCI name="account-outline" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10, marginTop: 5, }}>Account</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PrivacyPolicy')}>
                    <View style={{ flexDirection: 'row' }}>
                        <MCI name="shield-account" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10, marginTop: 5 }}>Privacy Policy</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row' }}>
                        <Ionicons name="help-buoy" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10, marginTop: 5 }}>Help</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row' }}>
                        <Feather name="info" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10, marginTop: 5 }}>About</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <View style={{ flexDirection: 'row' }}>
                        <AntDesign name="copy1" size={30} color="gold" />
                        <Text style={{ color: 'white', marginLeft: 10, marginTop: 5 }}>Term and Condition</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrap: {
        alignSelf: 'center',
        backgroundColor: '#818181',
        paddingHorizontal: 20,
        width: '90%',
        borderRadius: 15
    },
    button: {
        alignSelf: 'center',
        backgroundColor: '#818181',
        marginTop: 15,
        width: '90%',
        borderRadius: 15,
        padding: 15,
    },
})
