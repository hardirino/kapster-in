import React, {Component} from 'react';
import {Image, StyleSheet, View, StatusBar} from 'react-native';
import imgSplash from '../../Images/splashImage.png';
import {connect} from 'react-redux';

class SplashScreen extends Component {
  componentDidMount() {
    const {navigation, dataUser} = this.props;
    setTimeout(() => {
      if (dataUser.isLogin === true) {
        if (dataUser.dataUser.role === 'kapster') {
          navigation.replace('KapsterHome');
        } else if (dataUser.dataUser.role === 'user') {
          navigation.replace('MainHome');
        }
      } else {
        navigation.replace('Login');
      }
    }, 3000);
  }
  render() {
    return (
      <View>
        <Image style={styles.imageBackground} source={imgSplash} />
        <StatusBar hidden />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataUser: state.userReducer,
  };
};

export default connect(mapStateToProps)(SplashScreen);

const styles = StyleSheet.create({
  imageBackground: {
    resizeMode: 'stretch',
    width: '100%',
    height: '100%',
  },
  logo: {},
});
