import { Image, TouchableOpacity, StyleSheet, ImageBackground, Text, View } from 'react-native'
import React, { Component } from 'react'
import Logo from '../../Images/Logo-Gold.png'
import Icon from 'react-native-vector-icons/AntDesign'
import { colors } from '../../utils';


export default class PrivacyPolicy extends Component {
    render() {
        const {navigation} = this.props;
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkGrey, }} >
                <View style={{ flexDirection: 'row', alignItems: 'flex-start', marginLeft: 10, marginTop: 15, marginBottom: 10, }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name="left" size={30} style={{ color: 'gold' }} />
                    </TouchableOpacity>
                    <Text style={{ color: 'gold', fontSize: 15, marginTop: 5, marginLeft: 110, fontWeight: 'bold' }}> Privacy Policy </Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Image source={Logo} style={{ width: 100, height: 100, marginVertical: 50, }} />
                </View>
                <Text style={styles.text}>
                    When you use our services, you’re trusting us with your information. We understand this is a big responsibility and work hard to protect your information and put you in control.
                    This Privacy Policy is meant to help you understand what information we collect, why we collect it, and how you can update, manage, export, and delete your information.
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        marginTop:20,
        color:'white',
        alignSelf:'center',
        justifyContent:'center',
        // width:'80%',
        // height:'80%',
        fontSize:16,
        fontWeight:'bold'
    }
})