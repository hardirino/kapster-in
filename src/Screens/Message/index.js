import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import firestore from '@react-native-firebase/firestore';
import {connect} from 'react-redux';
import {colors, fonts} from '../../utils';
import {CCardChat} from '../../Components';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';


class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataChat: [],
    };
  }

    componentDidMount() {
        const { dataUser } = this.props;
        firestore()
            .collection('messages')
            .doc(dataUser.uid)
            .collection('chatWith')
            .onSnapshot(res => {
                const data = res.docs.map(item => {
                    return { messages: item.data().messages, ...item.data().lastChat };

                });
                // console.log(data[0].messages)
                this.setState({ dataChat: data })
            });
    }


    _delete = () => {
        this.setState({ dataChat: [] });
    };

    render() {
        const { navigation } = this.props;
        const { dataChat } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkGrey }}>
                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 15, marginBottom: 10, }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 15, marginTop: 5, marginLeft: 150, fontWeight: 'bold' }}> Message </Text>
                    <MCI
                        name='delete-alert'
                        style={{ marginLeft: '30%' }}
                        size={30}
                        color={'gold'}
                        onPress={() => this._delete()} />
                </View>
                {dataChat ? (
                    dataChat.map((value, index) => {
                        return (
                            <View style={styles.wrap} key={index}>
                                <CCardChat
                                    name={value.name}
                                    message={value.messages[value.messages.length - 1].text}
                                    onPress={() => {
                                        navigation.navigate('onChat', value);
                                    }}
                                />
                            </View>
                        );
                    })
                ) : (
                    <Text>No messages</Text>
                )}
            </View>
        )
    }
}

const mapStateToProps = state => {
  return {
    dataUser: state.userReducer.dataUser,
  };
};

const mapDispatchToProps = send => {
  return {
    chatData: data =>
      send({
        type: 'CHAT-DATA',
        payload: data,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Message);
const styles = StyleSheet.create({
  text: {
    fontFamily: fonts.semiBold,
    fontSize: 20,
    marginBottom: 20,
  },
  wrap: {
    flexDirection: 'row',
    padding: 10,
    width: '90%',
    height: 75,
  },
});
