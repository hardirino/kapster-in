import React, {Component} from 'react';
import {
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Text,
  Alert,
  View,
} from 'react-native';
import MaterialComunnityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import logo from '../../Images/Logo-Gold.png';
import {colors, fonts} from '../../utils';

export default class RegisterKapster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
    };
  }

  _onContinue = () => {
    const {name, email, password} = this.state;
    const {navigation} = this.props;
    if (name && email && password) {
      navigation.navigate('RegisterKapsterInfo', this.state);
    } else {
      Alert.alert('Error', 'You need to input all of them');
    }
  };

  render() {
    const {name, email, password} = this.state;
    const {navigation} = this.props;
    return (
      <View style={styles.mainContainer}>
        <MaterialComunnityIcon
          style={styles.arrowBack}
          name={'chevron-left'}
          size={40}
          color={colors.gold}
          onPress={() => navigation.goBack()}
        />
        <Image style={styles.logo} resizeMode={'contain'} source={logo} />
        <Text style={styles.textWelcome}>WELCOME TO KAPSTERIN</Text>
        <Text style={styles.textCreate}>
          Create your account and become a Kapster
        </Text>
        <View style={styles.inputParentWrapper}>
          <Text style={{...styles.textCreate, color: colors.darkGrey}}>
            Please fill out this field
          </Text>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'account'}
              size={35}
              color={colors.darkGrey}
              style={{marginLeft: 15}}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Full Name"
              value={name}
              onChangeText={name => this.setState({name})}
            />
          </View>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'email'}
              size={35}
              color={colors.darkGrey}
              style={{marginLeft: 15}}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Email"
              value={email}
              onChangeText={email => this.setState({email})}
              keyboardType="email-address"
            />
          </View>
          <View style={styles.inputWrapper}>
            <MaterialComunnityIcon
              name={'lock'}
              size={35}
              color={colors.darkGrey}
              style={{marginLeft: 15}}
            />
            <TextInput
              style={styles.inputText}
              placeholder="Password"
              secureTextEntry
              value={password}
              onChangeText={password => this.setState({password})}
            />
          </View>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this._onContinue();
          }}>
          <Text style={styles.buttonText}>Next</Text>
          <MaterialComunnityIcon
            name={'chevron-right'}
            size={35}
            color={colors.darkGrey}
          />
        </TouchableOpacity>
        <Text style={styles.textSignIn}>
          Already have an account?{' '}
          <Text
            onPress={() => navigation.navigate('Login')}
            style={{color: colors.red}}>
            Sign In!
          </Text>
        </Text>
        <Text style={{...styles.textSignIn, marginTop: 5}}>
          Register as a{' '}
          <Text
            onPress={() => navigation.navigate('Register')}
            style={{color: colors.red}}>
            User!
          </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.darkGrey,
    alignItems: 'center',
  },
  arrowBack: {
    marginLeft: 10,
    marginTop: 20,
    alignSelf: 'flex-start',
  },
  logo: {
    height: '20%',
    width: '30%',
  },
  textWelcome: {
    fontSize: 23,
    color: colors.gold,
    marginTop: 20,
    fontFamily: fonts.bold,
  },
  textCreate: {
    color: colors.gold,
    fontFamily: fonts.regular,
  },
  inputParentWrapper: {
    width: '90%',
    height: 300,
    alignItems: 'center',
    backgroundColor: colors.gold,
    borderRadius: 15,
    marginTop: 20,
    paddingVertical: 15,
  },
  inputWrapper: {
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: colors.darkGrey,
    width: '87%',
  },
  inputText: {
    fontSize: 23,
    color: colors.darkGrey,
    marginLeft: 10,
    fontFamily: fonts.regular,
    width: '80%',
  },
  button: {
    backgroundColor: colors.gold,
    width: '25%',
    height: '5%',
    borderRadius: 10,
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
    flexDirection: 'row',
  },
  buttonText: {
    fontSize: 20,
    color: colors.darkGrey,
    fontFamily: fonts.bold,
    marginLeft:13,
    marginBottom:3,
  },
  textSignIn: {
    color: colors.white,
    marginTop: 40,
    fontFamily: fonts.semiBold,
  },
});
