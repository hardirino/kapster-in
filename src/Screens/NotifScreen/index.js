import React, { Component } from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { CNotification } from '../../Components';
import { colors, fonts, toastAndroidCenter } from '../../utils';
import firestore from '@react-native-firebase/firestore';
import { convertTime } from '../../utils/util/date';

class NotifScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    const { dataUser } = this.props;
    firestore()
      .collection('orders')
      .where('idKapster', '==', dataUser.uid)
      .onSnapshot(res => {
        let data = res.docs.map(getData => {
          return getData.data();
        });
        this.setState({
          orders: data,
        });
      });
  }

  _finish = data => {
    this.setState({ isLoading: true });
    firestore()
      .collection('orders')
      .doc(data.ordersID)
      .update({
        isDone: true,
      })
      .then(() => {
        this.setState({ isLoading: false });
        toastAndroidCenter('Barbering Completed!');
      });
  };

  render() {
    const { orders, isLoading } = this.state;
    return (
      <View style={{ backgroundColor: colors.darkGrey ,height:'100%'}}>
        <Text style={styles.text}> Inbox </Text>
        <ScrollView style={styles.pages} showsVerticalScrollIndicator={false}>
          {orders.map((value, index) => {
            return (
              <CNotification
                name={value.user}
                image={value.paymentImage}
                time={convertTime(value.dateOrder.toDate())}
                onPress={() => this._finish(value)}
                key={index}
                isDone={value.isDone}
                isLoading={isLoading}
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataUser: state.userReducer.dataUser,
  };
};

export default connect(mapStateToProps)(NotifScreen);

const styles = StyleSheet.create({
  pages: {
    flex:1,
    paddingHorizontal: 17,
    // paddingTop: 15,
    backgroundColor: colors.darkGrey,
    // position:'absolute',
    // top:20,
    marginBottom:70,
  },
  text: {
    fontFamily: fonts.bold,
    fontSize: 20,
    // marginBottom: 20,
    marginTop: 20,
    alignSelf: 'center'
  },
});
