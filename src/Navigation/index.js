import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  Home,
  Search,
  Setting,
  Splash,
  Register,
  Login,
  ForgetPassword,
  Message,
  User,
  RegisterKapster,
  RegisterKapsterInfo,
  PrivacyPolicy,
  Account,
  Chat,
  NotifScreen,
  OrderKapster,
  onChat,
} from '../Screens'
import { TabNavigation, KapsterNavigation } from '../Components/Huge';


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const MainHome = () => {
  return (
    <Tab.Navigator tabBar={props => <TabNavigation {...props} />}>
      <Tab.Screen name="Home" component={Home} options={{ statusBarHidden: true, headerShown: false }} />
      <Tab.Screen name="Search" component={Search} options={{ statusBarHidden: true, headerShown: false }} />
      <Tab.Screen name="Message" component={Message} options={{ statusBarHidden: true, headerShown: false }} />
      <Tab.Screen name="Setting" component={Setting} options={{ statusBarHidden: true, headerShown: false }} />
    </Tab.Navigator>
  );
};

const KapsterHome = () => {
  return (
    <Tab.Navigator tabBar={props => <KapsterNavigation {...props} />}>
      <Tab.Screen name="Message" component={Message} options={{ statusBarHidden: true, headerShown: false }} />
      <Tab.Screen name="NotifScreen" component={NotifScreen} options={{ statusBarHidden: true, headerShown: false }} />
      <Tab.Screen name="Setting" component={Setting} options={{ statusBarHidden: true, headerShown: false }} />
    </Tab.Navigator>
  );
};

function index() {
  return (
    <Stack.Navigator initialRouteName='Kapster'>
      <Stack.Screen name="Splash" component={Splash} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="Register" component={Register} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="Login" component={Login} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="ForgetPassword" component={ForgetPassword} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="MainHome" component={MainHome} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="KapsterHome" component={KapsterHome} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="Search" component={Search} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="Message" component={Message} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="User" component={User} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="Account" component={Account} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="RegisterKapster" component={RegisterKapster} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="RegisterKapsterInfo" component={RegisterKapsterInfo} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="OrderKapster" component={OrderKapster} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="Chat" component={Chat} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="NotifScreen" component={NotifScreen} options={{ statusBarHidden: true, headerShown: false }} />
      <Stack.Screen name="onChat" component={onChat} options={{ statusBarHidden: true, headerShown: false }} />
    </Stack.Navigator>
  );
}

export default index;
