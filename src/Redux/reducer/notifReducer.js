const initialState = {
    inbox: []
};

const notifReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'INBOX':
            return {
                ...state,
                inbox: [action.payload, ...state.inbox].slice(0, 10)
            };
        default:
            return state;
    }
}

export default notifReducer;