import {combineReducers} from 'redux';
import userReducer from './userReducer';
import chatReducer from './chatReducer';
import notifReducer from './notifReducer';

const reducer = combineReducers({
  userReducer,
  chatReducer,
  notifReducer,
});



export default reducer;
