import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { SliderBox } from 'react-native-image-slider-box';
import { Kapster1, Kapster2 } from '../../../Assets/images';
import { colors } from '../../../utils';

export default class BannerSlider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            images: [Kapster1, Kapster2],
        };
    }

    render() {
        const { images } = this.state;
        return (
            <View style={styles.container}>
                <SliderBox
                    images={images}
                    autoplay
                    circleLoop
                    sliderBoxHeight={'100%'}
                    ImageComponentStyle={styles.slider}
                    dotStyle={styles.dotStyle}
                    dotColor={colors.gold}
                    imageLoadingColor={colors.primary}
                    parentWidth={365}            
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop:10,
    },
    slider: {
        borderRadius: 15,
    },
    dotStyle: {
        width: 10,
        height: 5,
        borderRadius: 5,
    },
});
