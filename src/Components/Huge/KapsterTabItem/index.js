import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors} from '../../../utils';



const KapsterTabItem = ({ isFocused, onPress, onLongPress, label, }) => {
  const Iconic = () => {

    if (label === 'NotifScreen') {
      return isFocused ? (
        <Ionicons name={'notifications'} size={35} color={colors.gold} />
      ) : (
        <Ionicons name={'notifications'} size={35} color={colors.white} />
      );
    }
    if (label === 'Message') {
      return isFocused ? (
        <AntDesign name={'message1'} size={35} color={colors.gold} />
      ) : (
        <AntDesign name={'message1'} size={35} color={colors.white} />
      );
    }
    if (label === 'Setting') {
      return isFocused ? (
        <AntDesign name={'setting'} size={35} color={colors.gold} />
      ) : (
        <AntDesign name={'setting'} size={35} color={colors.white} />
      );
    }

    return <Ionicons name={'notifications'} size={35} color={colors.white} />

  };

  return (
    <View>
      <TouchableOpacity
        accessibilityState={isFocused ? { selected: true } : {}}
        onPress={onPress}
        onLongPress={onLongPress}
        style={styles.container}>
        <Iconic />
      </TouchableOpacity>
    </View>
  );
};

export default KapsterTabItem;

const styles = StyleSheet.create({
  text: isFocused => ({
    color: isFocused ? '#DECB22' : '#C4C4C4',
    fontSize: 11,
    marginTop: 4,
  }),
});
