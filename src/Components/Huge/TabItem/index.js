import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {colors} from '../../../utils';

const TabItem = ({isFocused, onPress, onLongPress, label}) => {
  const Iconic = () => {
    if (label === 'Home') {
      return isFocused ? (
        <AntDesign name={'home'} size={35} color={colors.gold} />
      ) : (
        <AntDesign name={'home'} size={35} color={colors.white} />
      );
    }
    if (label === 'Search') {
      return isFocused ? (
        <AntDesign name={'search1'} size={35} color={colors.gold} />
      ) : (
        <AntDesign name={'search1'} size={35} color={colors.white} />
      );
    }
    if (label === 'Message') {
      return isFocused ? (
        <AntDesign name={'message1'} size={35} color={colors.gold} />
      ) : (
        <AntDesign name={'message1'} size={35} color={colors.white} />
      );
    }
    if (label === 'Setting') {
      return isFocused ? (
        <AntDesign name={'setting'} size={35} color={colors.gold} />
      ) : (
        <AntDesign name={'setting'} size={35} color={colors.white} />
      );
    }

    return <AntDesign name={'home'} size={35} color={colors.white} />;
  };

  return (
    <View>
      <TouchableOpacity
        accessibilityState={isFocused ? {selected: true} : {}}
        onPress={onPress}
        onLongPress={onLongPress}>
        <Iconic />
      </TouchableOpacity>
    </View>
  );
};

export default TabItem;
