import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class CLayout extends Component {
    render() {
        const { style } = this.props;
        return (
            <View {...this.props} style={{ ...styles.layout, ...style }}>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    layout: {
        backgroundColor: '#632626',
        borderRadius:10,
        borderWidth: 5,
        borderColor:'pink',
        height:605
    }
})