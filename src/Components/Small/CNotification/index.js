import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CGap from '../CGap';
import {
  IconTask,
  IconTime,
  IconUserBlue,
} from '../../../Assets';
import { colors, fonts, responsiveHeight, responsiveWidth } from '../../../utils';
import CButton from '../CButton';

export default class CNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { name, time, onPress, image, isDone } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ alignSelf: 'center' }}>
          <Text style={{ ...styles.textBold, marginLeft: 0 }}>New Order Booking</Text>
        </View>
        <CGap height={15} />
        <View>
          <View style={styles.wrapper}>
            <IconUserBlue />
            <View style={styles.wrapperText}>
              <Text style={styles.textRegular}>From</Text>
              <Text style={styles.textBold}>{name}</Text>
            </View>
          </View>
          <View style={styles.wrapper}>
            <IconTime />
            <View style={styles.wrapperText}>
              <Text style={styles.textRegular}>Order Time</Text>
              <Text style={styles.textBold}>{time}</Text>
            </View>
          </View>
          <CGap height={10} />
          {isDone ? (
            <CButton
              backgroundColor='green'
              text="Done Barbering." />
          ) : (
            <CButton
              backgroundColor='red'
              text="Press to Confirm Barber Order!"
              onPress={onPress}
            />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRightWidth: 0.3,
    borderBottomWidth: 0.3,
    borderLeftWidth: 0.3,
    borderColor: colors.gray,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    shadowColor: '#000',
    justifyContent: 'space-between',
    marginBottom: 20,
    top: 10,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
  },
  textBold: {
    fontFamily: fonts.semiBold,
    fontSize: 15,
    marginLeft: 20,
  },
  textRegular: {
    fontFamily: fonts.regular,
    fontSize: 12,
    marginLeft: 20,
    color: colors.gray,
  },
  wrapperImage: {
    height: responsiveHeight(250),
    width: responsiveWidth(360),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.2,
  },
});
