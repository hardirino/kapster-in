import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {getHour} from '../../../utils/util/date';

export default class CBubbleText extends Component {
  render() {
    const {isMe, text, time} = this.props;
    return (
      <View
        style={[
          styles.container,
          {
            alignSelf: isMe ? 'flex-end' : 'flex-start',
            backgroundColor: isMe ? colors.gold : colors.grey,
          },
        ]}>
        <Text style={styles.text}> {text} </Text>
        <Text style={styles.time}>{getHour(time)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    elevation: 4,
    marginBottom: 20,
    borderRadius: 10,
    maxWidth: '80%',
    minWidth:'30%'
  },
  text: {
    fontFamily: fonts.regular,
    fontSize: 15,
    color: colors.darkGrey,
  },
  time: {
    fontSize: 10,
    fontFamily: fonts.light,
    marginTop:5,
    alignSelf:'flex-end',
    color:colors.darkGreyTransparent
  },
});
