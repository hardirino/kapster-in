import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export default class CLayout extends Component {
    render() {
        const { style } = this.props;
        return (
            <View {...this.props} style={{ ...styles.layout, ...style }}>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    layout: {
        backgroundColor: '#333333',
        flex: 1,
    }
})