import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors, fonts } from '../../../utils';
import Logout from 'react-native-vector-icons/MaterialIcons';


const CCardMenu = ({ menu, onPress }) => {
  return (
    <TouchableOpacity style={styles.setting} onPress={onPress}>
      <View style={styles.iconText}>
        <Text style={styles.text}>{menu.name}</Text>
      </View>
      <Logout name="logout" size={30} color="#333333" />
    </TouchableOpacity>
  );
};

// borderRadius: 5,
//                             backgroundColor: 'red',
//                             marginTop: 43,
export default CCardMenu;

const styles = StyleSheet.create({
  setting: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingVertical: 13,
    marginVertical: 2,
    backgroundColor:'gold',
    marginTop:20,
    width:'30%',
    borderRadius:20,

  },
  wrapperIcon: {
    backgroundColor: colors.lightBlue,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  iconText: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontFamily: fonts.regular,
    fontSize: 14,
    color:'#333333',
    fontWeight:'bold', marginRight:10,
  },
});
