import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import CImageCircle from '../CImageCircle';
import {colors, fonts} from '../../../utils';

const CCardChat = ({onPress, name, message, image}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={{flexDirection: 'row'}} onPress={onPress}>
        <View style={{width: '20%'}}>
          <CImageCircle
            image={
              image
                ? {uri: image}
                : require('../../../Assets/images/kapster2.jpg')
            }
          />
        </View>
        <View style={styles.desc}>
          <View style={styles.nameDate}>
            <Text style={styles.name}>{name}</Text>
          </View>
          <Text style={styles.dateMessage}>{message}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default CCardChat;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: colors.gray,
    marginLeft: 15,
  },
  desc: {
    width: '80%',
    paddingLeft:8,
  },
  nameDate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  name: {
    paddingTop:5,
    paddingBottom:5,
    fontFamily: fonts.medium,
    fontSize: 15,
  },
});
